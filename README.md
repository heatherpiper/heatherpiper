### Welcome!

My name's Heather, and I'm a recent grad of Tech Elevator's full-stack Java bootcamp in Columbus, OH.

At present, I've been dipping my toes into all sorts of new languages, tools, and frameworks, though I have a particular fondness for customization of all varieties. I am in the process of developing custom themes for IntelliJ IDEA and Visual Studio Code that I hope to publish soon.  

I'm interested in data analytics, AI, crypto, and UX/UI - basically, I'm eager to try everything, develop my skills, and see where this new career trajectory will land me. 
